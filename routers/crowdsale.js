const express = require('express');


const router = express.Router();

router.use('/', express.static(process.cwd()));

module.exports = router;

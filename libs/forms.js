(() => {
    const forms = document.querySelectorAll('form');
    if(!forms) {
        return;
    }
    for(let form of forms){
        const fieldNamesMapping = new Map([
            //['formElementName', 'requestFieldName'],
        ]);
        $('form').on('submit', function(event) {
            const form = $(this);
            event.preventDefault();
            let data = {
                name: 'Join ICO Popup Form',
                source: window.location.origin, 
                fields: (
                    window.location.search.
                    slice(1).split('&').
                    reduce((fields, pair) => {
                        const [name, value] = decodeURIComponent(pair).split('=');
                        fields[name.toLowerCase()] = value;
                        return fields;
                    }, {})
                )
            };
            for(let {name, value} of form.serializeArray()){
                let option = form.find(`select[name="${name}"] option:selected`);
                let mapped = fieldNamesMapping.get(name.toLowerCase());
                data.fields[mapped||name] = option.length ? option.text() : value;
            }
            $.ajax({
                data,
                url: '//cubegroup.services/form/revain',
                type: 'POST',
            }).then(
            response => {},
            err => {
                console.error(err.statusText);
            })
        });
    }
})();

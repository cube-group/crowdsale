import "./forms";
import "./metrics";
import "./jquery.easing.min.js";
import "./waypoints.min.js";
import "./magnific.popup.min.js";
import "./countdown.js";
import "./progress.js";
import "./chart.js";
import "./img.picker.js";
import "./sweetalert.min.js";


$(document).ready(function() {

		$('.toProfile').click(function() {
			swal({
			  title: "Congratulations!",
			  text: "Soon you will receve a massage with access to your profile. Do not lose your link! Click 'OK' to go to profile!",
			  type: "info",
			  showCancelButton: true,
			  confirmButtonColor: "#8cd4f5",
			  confirmButtonText: "OK",
			  cancelButtonText: "cancel",
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm){
			  if (isConfirm) {
			    swal("Good luck!", "We will send you Link", "success");
			  } else {
			    swal("Cancelled", "Your Link to Profile delted!", "error");
			  }
			});
		});


	$('.popup-with-move-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom'
	});

  (function($){

    $('#header__icon').click(function(e){
        e.preventDefault();
        $('body').toggleClass('with--sidebar');
    });

    $('#site-cache').click(function(e){
      $('body').removeClass('with--sidebar');
    });

  })(jQuery);

  //Scroll Menu

	$(window).on('scroll', function(){
		if( $(window).scrollTop()>60 ){
			$('.header').addClass('fixed animated fadeInDown');
		} else {
			$('.header').removeClass('fixed animated fadeInDown');
		}
	});

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $(function() {
      $('.overlay-menu a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
      });
    });


    $("#lang").change(function() {
      window.location = $(this).find("option:selected").val();
    });

    function DropDown(el) {
      this.dd = el;
      this.initEvents();
    }

    DropDown.prototype = {
      initEvents: function () {
        var obj = this;
        // Determine if dropdown is on hover or on click
        if ($(".dropdown").hasClass("dropdown-hover")) {
          // Toggle .dropdown-active on hover
          $(".dropdown").mouseenter(function (event) {
              $(this).addClass("dropdown-active");
              event.stopPropagation();
          });

          $(".dropdown-menu").mouseleave(function () {
              $(".dropdown").removeClass("dropdown-active");
          });
        } else {
          // Toggle .dropdown-active on click
          obj.dd.on('click', function (event) {
              $(this).toggleClass('dropdown-active');
              event.stopPropagation();
          });
        }
      }
    }

    $(function () {
      var dd = new DropDown($('.dropdown'));
      $('.dropdown').click(function () {
          // Remove class from all dropdowns
          $('.dropdown').toggleClass('dropdown-active');
      });
    });


});


$(document).ready(function() {
    // Progress Bar
    	$('#sample_goal').goalProgress({
    		goalAmount: 10000,
    		currentAmount: 8000,
    		textBefore: '',
    		textAfter: ' BTC'
    	});

    $("select").imagepicker({
          hide_select : true,
          show_label  : true
        })
      // Count Down
      $('#getting-started').countdown('2017/08/16 12:34:56')
      .on('update.countdown', function(event) {
        var format = '%H:%M:%S';
        if(event.offset.totalDays > 0) {
          format = '%-d day%!d ' + format;
        }
        if(event.offset.weeks > 0) {
          format = '%-w week%!w ' + format;
        }
        $(this).html(event.strftime(format));
      })
      .on('finish.countdown', function(event) {
        $(this).html('Crowdsale has started!')
          .parent().addClass('disabled');
      });


      // First doughnut Chart
      var first = document.getElementById('first');
      var chart = new Chart(first, {
          // The type of chart we want to create
          type: 'doughnut',

          // The data for our dataset
          data: {
              labels: ['Russian Federation (RU)', 'USA (US)'],
              datasets: [{
                  label: 'First doughnut',
                  data: [83,17],
      						backgroundColor: ['rgb(127,170,136)', 'rgb(162,197,233)'],
      						borderColor: 'rgba(127,170,136,0)',
              }]
          },
          // Configuration options go here
          options: {
      			title: {
      				display: true,
      				text: 'Global investments',
      				fontSize: 23,
      				fontColor: '#ffffff',
      				fontFamily: "'Montserrat', sans-serif",
      				fontStyle: 'lighter',
      				padding: 30
      			},
      			legend: {
      				display: false
      			}
      		}
      });

      // Bar Chart
      var bar = document.getElementById('bar');
      var chart = new Chart(bar, {
          // The type of chart we want to create
          type: 'bar',

          // The data for our dataset
          data: {
              labels: ['DSH', 'NEM', 'BTC', 'ETH', 'LTC', 'ZEC'],
              datasets: [{
                  // label: 'First doughnut',
                  data: [1.5,3,55,38,2,0.5],
      						backgroundColor: ['rgb(244,91,95)','rgb(233,162,215)', 'rgb(220,233,162)', 'rgb(102,106,101)', 'rgb(162,229,233)', 'rgb(127,170,136)'],
      						borderColor: 'rgba(127,170,136,0)',
              }]
          },
          // Configuration options go here
          options: {
      			title: {
      				display: true,
      				text: 'Cryptocurrency Invested',
      				fontSize: 23,
      				fontColor: '#ffffff',
      				position: 'bottom',
      				fontFamily: "'Montserrat', sans-serif",
      				fontStyle: 'lighter'
      			},
      			legend: {
      				display: false
      			},
      			scales: {
      	      xAxes: [{
      	        gridLines: {
      	          drawOnChartArea: false
      	        }
      	      }],
      				yAxes: [{
      	        gridLines: {
      	          drawOnChartArea: false
      	        }
      	      }]
      	    }
      		}
      });
});

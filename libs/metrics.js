(() => {
    const targets = document.querySelectorAll('[data-metrics-goal]');
    for(let target of targets){
        target.addEventListener('click', () => {
            const goal = target.getAttribute('data-metrics-goal');
            yaCounter45594834.reachGoal(goal);
            ga('send', 'event', 'btn', 'click', goal);
        });
    }
})();

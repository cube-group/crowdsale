const express = require('express');
const {createServer } = require('http');

const { config: {host, port} } = require('./package.json');

const { crowdsale } = require('./routers');


const app = express();
app.use('/', crowdsale);

createServer(app).
listen(port, host, (err) => {
    if(err){
        throw err;
        process.exit();
    } else {
        console.info(`started http server at ${host}:${port}`);
    }
});

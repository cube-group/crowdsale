const webpack = require('webpack');


module.exports = {
  entry: './libs/scripts.js',
  output: {
    filename: './libs/webpack.bundle.js'
  },
  module: {
        
 },
  plugins: [
       new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        })
    ],
}
